package de.foodsharing.ui.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.R
import de.foodsharing.api.PopupAPI
import de.foodsharing.api.UserResponse
import de.foodsharing.services.AuthService
import de.foodsharing.test.configureTestSchedulers
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.HttpException

class LoginViewModelTest {

    private lateinit var viewModel: LoginViewModel

    @Mock
    lateinit var auth: AuthService

    @Mock
    lateinit var popupAPI: PopupAPI

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()

        whenever(popupAPI.current(any())) doReturn Observable.empty()

        viewModel = LoginViewModel(auth, popupAPI)
    }

    @Test
    fun testLogin() {
        val testSubject = PublishSubject.create<UserResponse>()
        whenever(auth.login(any())).thenReturn(testSubject)

        viewModel.login("test", "1234")

        assertEquals(true, viewModel.isLoading.value)
        testSubject.onNext(UserResponse(1, "test"))
        assertEquals(false, viewModel.isLoading.value)
        assertEquals(1, viewModel.loginFinished.value?.peekContent())
        assertEquals(null, viewModel.showError.value)
    }

    @Test
    fun testLoginFailed() {
        val testSubject = PublishSubject.create<UserResponse>()
        whenever(auth.login(any())).thenReturn(testSubject)

        viewModel.login("test", "1234")

        assertEquals(true, viewModel.isLoading.value)
        val authException = Mockito.mock(HttpException::class.java)
        whenever(authException.code()).thenReturn(401)
        testSubject.onError(authException)
        assertEquals(false, viewModel.isLoading.value)
        assertEquals(null, viewModel.loginFinished.value)
        assertEquals(R.string.invalid_password, viewModel.showError.value?.peekContent())
    }
}