package de.foodsharing.services

import de.foodsharing.api.BasketAPI
import de.foodsharing.model.Coordinate
import de.foodsharing.utils.PICTURE_MIME_TYPE
import de.foodsharing.utils.Utils
import de.foodsharing.utils.testing.OpenForTesting
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class BasketService @Inject constructor(
    private val basketAPI: BasketAPI
) {
    val deletions = PublishSubject.create<Int>()
    val additions = PublishSubject.create<BasketAPI.BasketResponse>()
    val changes = PublishSubject.create<Pair<Int, BasketAPI.BasketResponse>>()
    val myBasketsChangeMapper = { baskets: BasketAPI.BasketListResponse ->
        // TODO find better way to keep the types
        Observable.merge(deletions, additions, changes).scanWith({
            baskets
        }, { t1, t2 ->
            val newBaskets = if (t2 is Int) {
                t1.baskets?.filter {
                    it.id != t2
                }
            } else if (t2 is BasketAPI.BasketResponse && t2.basket != null) {
                t1.baskets?.union(listOf(t2.basket!!))
            } else if (t2 is Pair<*, *>) {
                t1.baskets?.map {
                    if (it.id == t2.first) (t2.second as BasketAPI.BasketResponse).basket!! else it
                }
            } else {
                t1.baskets
            }
            t1.baskets = newBaskets?.toList()
            t1
        })
    }

    fun list(): Observable<BasketAPI.BasketListResponse> {
        return basketAPI.list().switchMap(myBasketsChangeMapper)
    }

    fun listClose(lat: Double, lon: Double, distance: Int): Observable<BasketAPI.BasketListResponse> {
        return basketAPI.listClose(lat, lon, distance)
    }

    fun remove(id: Int): Observable<Unit> {
        return basketAPI.remove(id).doOnComplete {
            deletions.onNext(id)
        }
    }

    fun get(id: Int): Observable<BasketAPI.BasketResponse> {
        return basketAPI.get(id).switchMap { basket ->
            Observable.just(basket).concatWith(changes.filter { it.first == basket.basket?.id }.map { it.second })
        }
    }

    fun create(
        description: String,
        contactTypes: Array<Int>,
        phone: String?,
        mobile: String?,
        lifetime: Int,
        lat: Double?,
        lon: Double?
    ): Observable<BasketAPI.BasketResponse> {
        return basketAPI.create(description, contactTypes, phone, mobile, lifetime, lat, lon).doOnNext {
            additions.onNext(it)
        }
    }

    fun setPicture(id: Int, file: File): Observable<BasketAPI.BasketResponse> =
        Observable.just(file).map {
            Utils.prepareImageFileForUpload(it)
        }.flatMap { picture ->
            basketAPI.setPicture(id, RequestBody.create(MediaType.get(PICTURE_MIME_TYPE), picture))
        }.doOnNext {
            changes.onNext(Pair(id, it))
        }

    fun update(
        id: Int,
        description: String,
        coordinate: Coordinate
    ): Observable<BasketAPI.BasketResponse> =
        basketAPI.update(id, description, coordinate.lat, coordinate.lon).doOnNext {
            changes.onNext(Pair(id, it))
        }
}
