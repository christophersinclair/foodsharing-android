package de.foodsharing.services

import com.franmontiel.persistentcookiejar.ClearableCookieJar
import de.foodsharing.api.AuthAPI
import de.foodsharing.api.LoginRequest
import de.foodsharing.api.UserResponse
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.utils.testing.OpenForTesting
import io.reactivex.Observable
import retrofit2.HttpException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class AuthService @Inject constructor(
    private val api: AuthAPI,
    private val ws: WebsocketAPI,
    private val cookieJar: ClearableCookieJar,
    private val preferences: PreferenceManager
) {

    @Volatile
    var currentUser: UserResponse? = null

    fun currentUser(): Observable<UserResponse> {
        return currentUser?.let {
            Observable.just(it)
        } ?: api.current().doOnNext {
            currentUser = it
        }
    }

    fun check(): Observable<Boolean> {
        return api.current()
                .doOnNext { user -> currentUser = user }
                .map { true }
                .onErrorReturn { error ->
                    if (error is HttpException && error.code() == 404) {
                        clear()
                        false
                    } else {
                        throw(error)
                    }
                }
    }

    fun login(data: LoginRequest): Observable<UserResponse> {
        return api.login(data)
            .doOnNext { user -> currentUser = user }
            .doOnNext { preferences.isLoggedIn = true }
    }

    fun logout(): Observable<Unit> {
        return api.logout().doFinally { clear() }
    }

    fun clear() {
        currentUser = null
        ws.close()
        cookieJar.clear()
        preferences.isLoggedIn = false
    }
}