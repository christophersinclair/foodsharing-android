package de.foodsharing.ui.basket

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.model.Basket
import de.foodsharing.services.AuthService
import de.foodsharing.services.BasketService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import retrofit2.HttpException
import javax.inject.Inject

class BasketViewModel @Inject constructor(
    private val basketService: BasketService,
    auth: AuthService
) : BaseViewModel() {
    var basketId: Int? = null
        set(value) {
            if (field != value) {
                field = value
                if (basket.value?.id != field) fetch()
            }
        }

    val isCurrentUser = MutableLiveData<Boolean>().apply { value = false }
    val isLoading = MutableLiveData<Boolean>()
    val showError = MutableLiveData<Event<Int>>()
    val basket = MutableLiveData<Basket>()
    val basketRemoved = MutableLiveData<Event<Unit>>()

    private var currentUserId: Int? = null

    init {
        request(auth.currentUser(), {
            currentUserId = it.id
            isCurrentUser.value = currentUserId != null && basket.value?.creator?.id == currentUserId
        })
    }

    private fun fetch() {
        if (basketId != null) {
            isLoading.value = true

            request(basketService.get(basketId!!), {
                isLoading.value = false
                basket.value = it.basket

                isCurrentUser.value =
                    currentUserId != null && basket.value?.creator?.id == currentUserId
            }, {
                isLoading.value = false
                handleError(it)
            })
        } else {
            basket.value = null
        }
    }

    fun removeBasket() {
        basket.value?.let {
            isLoading.value = true

            request(basketService.remove(it.id), {
                isLoading.value = false
                basketRemoved.value = Event(Unit)
            }, {
                isLoading.value = false
                handleError(it)
            })
        }
    }

    private fun handleError(error: Throwable) {
        val stringRes = if (error is HttpException && error.code() == 404) {
            R.string.basket_404
        } else {
            R.string.error_unknown
        }
        showError.value = Event(stringRes)
    }
}
