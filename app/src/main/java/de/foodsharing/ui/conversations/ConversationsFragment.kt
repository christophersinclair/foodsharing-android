package de.foodsharing.ui.conversations

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.stfalcon.chatkit.dialogs.DialogsListAdapter
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.ConversationListEntry
import de.foodsharing.model.Message
import de.foodsharing.model.User
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.conversation.ChatkitMessage
import de.foodsharing.ui.conversation.ConversationActivity
import de.foodsharing.utils.ChatkitPicassoImageLoader
import de.foodsharing.utils.withColor
import kotlinx.android.synthetic.main.fragment_conversation_list.no_conversations_label
import kotlinx.android.synthetic.main.fragment_conversation_list.progress_bar
import kotlinx.android.synthetic.main.fragment_conversation_list.pull_refresh
import kotlinx.android.synthetic.main.fragment_conversation_list.recycler_view
import kotlinx.android.synthetic.main.fragment_conversation_list.view.no_conversations_label
import kotlinx.android.synthetic.main.fragment_conversation_list.view.pull_refresh
import kotlinx.android.synthetic.main.fragment_conversation_list.view.recycler_view
import javax.inject.Inject

class ConversationsFragment : BaseFragment(),
        DialogsListAdapter.OnDialogClickListener<ChatkitConversation>,
        Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ConversationsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ConversationsViewModel::class.java)
    }

    private lateinit var adapter: FsDialogListAdapter
    private var errorSnackbar: Snackbar? = null

    // Number of items at the end of the list before starting to load the next page
    private val NUM_PREFETCH_ITEMS = 10

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_conversation_list, container, false)

        val layoutManager = LinearLayoutManager(activity)
        view.recycler_view.layoutManager = layoutManager
        adapter = FsDialogListAdapter(ChatkitPicassoImageLoader())
        adapter.setOnDialogClickListener(this)
        adapter.setDatesFormatter(ConversationDateFormatter(context!!))
        view.recycler_view.setAdapter(adapter)

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            view.recycler_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }

        view.pull_refresh.setOnRefreshListener {
            viewModel.refresh()
        }

        // listener that notices if the user scrolled to the bottom of the conversations list
        view.recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                val linLayoutManager = view.recycler_view.layoutManager as? LinearLayoutManager
                val remainingItems = adapter.itemCount - (linLayoutManager?.findLastVisibleItemPosition() ?: -1)
                if (remainingItems < NUM_PREFETCH_ITEMS) {
                    viewModel.loadNext()
                }
            }
        })

        view.no_conversations_label.movementMethod = LinkMovementMethod.getInstance()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        bindViewModel()
    }

    private fun bindViewModel() {
        val diff = AsyncListDiffer(adapter, object : DiffUtil.ItemCallback<ChatkitConversation>() {
            override fun areItemsTheSame(oldItem: ChatkitConversation, newItem: ChatkitConversation): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ChatkitConversation, newItem: ChatkitConversation): Boolean {
                return oldItem == newItem
            }
        })

        viewModel.conversationsWithCurrentUser.observe(this, Observer {
            val conversations = it.first ?: return@Observer
            val currentUser = it.second ?: return@Observer

            val updatedList = conversations.map { c ->
                // TODO refactor this with the refactor of the messaging module
                val u = findUserInConversation(c, c.lastFoodsaverID) ?: User(c.lastFoodsaverID, "", "", null, null)
                val msg = Message(-1, u.id, u.name, c.lastMessage!!, c.lastTS)
                ChatkitConversation(c, ChatkitMessage(msg, u), currentUser.id)
            }
            adapter.setItemsWithoutNotify(updatedList)
            diff.submitList(updatedList)

            no_conversations_label.visibility = if (conversations.isEmpty()) {
                View.VISIBLE
            } else {
                View.GONE
            }

            recycler_view.visibility = if (conversations.isEmpty()) {
                View.GONE
            } else {
                View.VISIBLE
            }
        })

        viewModel.isLoading.observe(this, Observer<Boolean> {
            progress_bar.visibility = if (it) View.VISIBLE else View.GONE
        })

        viewModel.isReloading.observe(this, Observer<Boolean> {
            pull_refresh.isRefreshing = it
        })

        viewModel.errorState.observe(this, Observer<Int> {
            errorSnackbar?.dismiss()
            val rootView = view
            val fragmentContext = context
            if (it != null && rootView != null && fragmentContext != null) {
                errorSnackbar = Snackbar.make(
                    rootView,
                    it,
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAction(R.string.retry_button) {
                        viewModel.tryAgain()
                    }.setActionTextColor(ContextCompat.getColor(fragmentContext, R.color.white))
                    .withColor(ContextCompat.getColor(fragmentContext, R.color.colorAccent))
                errorSnackbar?.show()
            }
        })
    }

    /**
     * Finds and returns the user with the specified id in the conversation, or null if not found.
     */
    private fun findUserInConversation(conversation: ConversationListEntry, id: Int): User? {
        return conversation.members.firstOrNull { it.id == id }
    }

    override fun onDialogClick(conversation: ChatkitConversation) {
        // show conversation
        val intent = Intent(context, ConversationActivity::class.java)
        intent.putExtra(ConversationActivity.EXTRA_CONVERSATION_ID, conversation.conversation.id)
        startActivity(intent)
        activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        adapter.setOnDialogClickListener(null)
        errorSnackbar?.dismiss()
        errorSnackbar = null
    }
}