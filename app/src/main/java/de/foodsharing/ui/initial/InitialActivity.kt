package de.foodsharing.ui.initial

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.services.AuthService
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.login.LoginActivity
import de.foodsharing.ui.main.MainActivity
import de.foodsharing.utils.ConnectivityReceiver
import de.foodsharing.utils.SHARED_PREFERENCE_LOGGED_IN
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.error_state.error_text_view
import kotlinx.android.synthetic.main.error_state.error_view
import kotlinx.android.synthetic.main.error_state.retry_button
import javax.inject.Inject

class InitialActivity : BaseActivity(), Injectable {

    @Inject
    lateinit var auth: AuthService

    @Inject
    lateinit var preferences: SharedPreferences

    private val subscriptions = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.error_state)

        subscriptions.add(auth.check()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retryWhen {
                    it.switchMap {
                        Observable.create<Unit> { e ->
                            error_view.visibility = View.VISIBLE

                            if (!this.isConnected) {
                                error_text_view.text = getText(R.string.error_no_connection)

                                ConnectivityReceiver.observe().filter { it }.firstElement().subscribe {
                                    if (!e.isDisposed && it) {
                                        e.onNext(Unit)
                                        error_view.visibility = View.GONE
                                        retry_button.setOnClickListener(null)
                                    }
                                }
                            } else {
                                error_text_view.text = getText(R.string.error_unknown)
                            }

                            retry_button.setOnClickListener {
                                e.onNext(Unit)
                                error_view.visibility = View.GONE
                                retry_button.setOnClickListener(null)
                            }
                            e.setCancellable {
                                error_view.visibility = View.GONE
                                retry_button.setOnClickListener(null)
                            }
                        }
                    }
                }
                .subscribe { isLoggedIn ->
                    preferences.edit().putBoolean(SHARED_PREFERENCE_LOGGED_IN, isLoggedIn).apply()
                    startActivity(Intent(
                            this,
                            if (isLoggedIn) MainActivity::class.java else LoginActivity::class.java
                    ))
                    finish()
                })
    }

    override fun onDestroy() {
        subscriptions.dispose()
        super.onDestroy()
    }
}
