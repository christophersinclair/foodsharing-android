package de.foodsharing.ui.editbasket

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.model.Coordinate
import de.foodsharing.ui.base.AuthRequiredBaseActivity
import de.foodsharing.ui.basket.PickLocationActivity
import de.foodsharing.ui.editbasket.EditBasketActivity.Companion.EXTRA_BASKET
import de.foodsharing.utils.DETAIL_MAP_ZOOM
import de.foodsharing.utils.OsmdroidUtils
import kotlinx.android.synthetic.main.activity_edit_basket.*
import javax.inject.Inject

/**
 * Activity that handles editing baskets. It requires a valid basket as an extra with
 * [EXTRA_BASKET].
 */
class EditBasketActivity : AuthRequiredBaseActivity(), EditBasketContract.View, Injectable {

    companion object {
        const val EXTRA_BASKET = "basket"

        private const val STATE_DESCRIPTION = "description"
        private const val STATE_COORDINATE = "coordinate"

        private const val REQUEST_PICK_LOCATION = 1
    }

    @Inject
    lateinit var presenter: EditBasketContract.Presenter

    private var basket: Basket? = null
    private var mapSnapshotDetachAction: (() -> Unit)? = null
    private lateinit var location: Coordinate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attach(this)
        rootLayoutID = R.id.edit_basket_root
        setContentView(R.layout.activity_edit_basket)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.basket_edit_title)

        basket = intent.getSerializableExtra(EXTRA_BASKET) as Basket

        basket?.let { updateBasketMap(it.toCoordinate()) }

        basket_update_button.setOnClickListener { updateBasket() }

        if (savedInstanceState == null) {
            basket_description_input.setText(basket?.description)
        } else if (savedInstanceState.containsKey(STATE_DESCRIPTION)) {
            basket_description_input.setText(savedInstanceState.getString(STATE_DESCRIPTION))
            updateBasketMap(savedInstanceState.getParcelable(STATE_COORDINATE))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onDestroy() {
        mapSnapshotDetachAction?.invoke()
        presenter.unsubscribe()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(STATE_DESCRIPTION, basket_description_input.text.toString())
        outState.putParcelable(STATE_COORDINATE, location)

        super.onSaveInstanceState(outState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_PICK_LOCATION -> {
                if (resultCode == RESULT_OK && data != null) {
                    data.getParcelableExtra<Coordinate?>(PickLocationActivity.EXTRA_COORDINATE)
                        ?.let { updateBasketMap(it) }
                }
            }
        }
    }

    override fun display(basket: Basket) {
        finish()
    }

    override fun showError(message: String) {
        progress_bar.visibility = GONE
        showMessage(message)
        basket_update_button.isEnabled = true
    }

    /**
     * Checks the input and updates the basket.
     */
    private fun updateBasket() {
        basket_update_button.isEnabled = false

        val description = basket_description_input.text.toString().trim()
        if (description.isEmpty()) {
            showMessage(getString(R.string.basket_error_description), Snackbar.LENGTH_LONG)
            basket_update_button.isEnabled = true
        } else {
            basket?.let {
                progress_bar.visibility = VISIBLE
                presenter.update(it.id, description, location)
            }
        }
    }

    private fun updateBasketMap(location: Coordinate) {
        this.location = location

        mapSnapshotDetachAction = OsmdroidUtils.loadMapTileToImageView(
            basket_location_view,
            location,
            DETAIL_MAP_ZOOM,
            BitmapFactory.decodeResource(resources, R.drawable.marker_basket)
        )

        basket_location_view.setOnClickListener {
            val intent = Intent(this, PickLocationActivity::class.java)
            intent.putExtra(PickLocationActivity.EXTRA_COORDINATE, location)
            intent.putExtra(PickLocationActivity.EXTRA_MARKER_ID, R.drawable.marker_basket)
            intent.putExtra(PickLocationActivity.EXTRA_ZOOM, DETAIL_MAP_ZOOM)
            startActivityForResult(intent, REQUEST_PICK_LOCATION)
        }
    }
}
