package de.foodsharing.ui.editbasket

import de.foodsharing.model.Coordinate
import de.foodsharing.services.BasketService
import de.foodsharing.ui.base.BasePresenter
import javax.inject.Inject

class EditBasketPresenter @Inject constructor(
    private val baskets: BasketService
) : BasePresenter<EditBasketContract.View>(), EditBasketContract.Presenter {

    override fun update(
        basketId: Int,
        description: String,
        coordinate: Coordinate
    ) {
        request(baskets.update(basketId, description, coordinate),
            {
                view?.display(it.basket!!)
            }, {
                view?.showError(it.localizedMessage)
            }
        )
    }
}