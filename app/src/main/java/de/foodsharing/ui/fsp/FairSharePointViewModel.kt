package de.foodsharing.ui.fsp

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.api.FairSharePointAPI
import de.foodsharing.model.FairSharePoint
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import retrofit2.HttpException
import javax.inject.Inject

class FairSharePointViewModel @Inject constructor(
    private val fspAPI: FairSharePointAPI
) : BaseViewModel() {
    var fairSharePointId: Int? = null
        set(value) {
            if (field != value) {
                field = value
                fetch()
            }
        }

    val isLoading = MutableLiveData<Boolean>().apply { value = true }
    val showError = MutableLiveData<Event<Int>>()
    val fairSharePoint = MutableLiveData<FairSharePoint>()

    private fun fetch() {
        if (fairSharePointId != null) {
            request(fspAPI.get(fairSharePointId!!).doOnNext {
                isLoading.postValue(true)
            }, {
                isLoading.value = false
                fairSharePoint.value = it
            }, {
                isLoading.value = false

                val stringRes = if (it is HttpException && it.code() == 404) {
                    R.string.fairsharepoint_404
                } else {
                    R.string.error_unknown
                }
                showError.value = Event(stringRes)
            })
        } else {
            fairSharePoint.value = null
        }
    }
}