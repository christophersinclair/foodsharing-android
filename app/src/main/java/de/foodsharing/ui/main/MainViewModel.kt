package de.foodsharing.ui.main

import androidx.lifecycle.MutableLiveData
import de.foodsharing.api.PopupAPI
import de.foodsharing.model.Popup
import de.foodsharing.services.AuthService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import de.foodsharing.utils.POPUP_CONFIG_ENDPOINT
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val auth: AuthService,
    private val popupAPI: PopupAPI
) : BaseViewModel() {
    val currentUserName = MutableLiveData<String>()

    val popup = MutableLiveData<Event<List<Popup>>>()

    init {
        request(auth.currentUser(), {
            currentUserName.value = it.name
        })

        request(popupAPI.current(POPUP_CONFIG_ENDPOINT), {
            popup.postValue(Event(it))
        }, {
            // Ignore error, popup will be shown on next launch
        })
    }

    fun logout() {
        // Do not autoDispose this call, to allow to finish the logout after the MainActivity closes
        request(auth.logout(), onError = {})
    }
}
