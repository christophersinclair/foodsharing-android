package de.foodsharing.ui.newbasket

import android.content.SharedPreferences
import de.foodsharing.model.Basket
import de.foodsharing.model.Coordinate
import de.foodsharing.services.BasketService
import de.foodsharing.services.ProfileService
import de.foodsharing.ui.base.BasePresenter
import java.io.File
import javax.inject.Inject

class NewBasketPresenter @Inject constructor(
    private val baskets: BasketService,
    private val profile: ProfileService,
    private val sharedPreferences: SharedPreferences
) : BasePresenter<NewBasketContract.View>(), NewBasketContract.Presenter {

    companion object {
        private const val LAST_CONTACT_BY_MESSAGE_PREF = "LAST_CONTACT_BY_MESSAGE_PREF"
        private const val LAST_CONTACT_BY_PHONE_PREF = "LAST_CONTACT_BY_PHONE_PREF"
    }

    override fun fetch() {
        val contactByMessage = sharedPreferences.getBoolean(LAST_CONTACT_BY_MESSAGE_PREF, false)
        val contactByPhone = sharedPreferences.getBoolean(LAST_CONTACT_BY_PHONE_PREF, false)

        request(profile.current(), { profile ->
            view?.showDefaults(profile, contactByMessage, contactByPhone)
        }, {
            // Ignore for now and pass null
            view?.showDefaults(null, contactByMessage, contactByPhone)
        })
    }

    override fun publish(
        description: String,
        phone: String?,
        mobile: String?,
        contactByMessage: Boolean,
        lifetime: Int,
        location: Coordinate?,
        picture: File?
    ) {
        val contactTypes = ArrayList<Int>()
        val contactByPhone = phone != null && mobile != null

        if (contactByPhone) contactTypes.add(Basket.CONTACT_TYPE_PHONE)
        if (contactByMessage) contactTypes.add(Basket.CONTACT_TYPE_MESSAGE)

        sharedPreferences.edit()
                .putBoolean(LAST_CONTACT_BY_PHONE_PREF, contactByPhone)
                .putBoolean(LAST_CONTACT_BY_MESSAGE_PREF, contactByMessage)
                .apply()

        request(
                baskets.create(
                        description,
                        contactTypes.toTypedArray(),
                        phone,
                        mobile,
                        lifetime,
                        location?.lat,
                        location?.lon
                ), {
            val basket = it.basket!!

            if (picture != null) {
                request(baskets.setPicture(basket.id, picture), {
                    view?.display(it.basket!!)
                }, {
                    view?.showError(it.localizedMessage)
                    view?.display(basket)
                })
            } else view?.display(basket)
        }, {
            view?.showError(it.localizedMessage)
        })
    }
}