package de.foodsharing.ui.newbasket

import de.foodsharing.api.Profile
import de.foodsharing.model.Basket
import de.foodsharing.model.Coordinate
import de.foodsharing.ui.base.BaseContract
import java.io.File

class NewBasketContract {

    interface View : BaseContract.View {

        /**
         * Called after the default values are fetch
         */
        fun showDefaults(profile: Profile?, contactByMessage: Boolean, contactByPhone: Boolean)

        /**
         * Called after a basket has been published.
         */
        fun display(basket: Basket)

        /**
         * Called if publishing failed.
         */
        fun showError(message: String)
    }

    interface Presenter : BaseContract.Presenter<View> {

        fun fetch()

        fun publish(
            description: String,
            phone: String?,
            mobile: String?,
            contactByMessage: Boolean,
            lifetime: Int,
            location: Coordinate?,
            picture: File?
        )
    }
}