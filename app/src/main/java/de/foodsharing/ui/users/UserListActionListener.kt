package de.foodsharing.ui.users

import de.foodsharing.model.User

interface UserListActionListener {
    fun onViewUser(user: User)
}