package de.foodsharing.ui.users

import android.graphics.Bitmap
import de.foodsharing.model.User
import de.foodsharing.ui.base.BaseContract

class UserListContract {

    interface View : BaseContract.View {
        fun displayUserPicture(user: User, picture: Bitmap)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun loadUserPicture(user: User)
    }
}