package de.foodsharing.ui.users

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.User
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.profile.ProfileActivity
import kotlinx.android.synthetic.main.activity_user_list.recycler_view
import kotlinx.android.synthetic.main.activity_user_list.toolbar
import javax.inject.Inject

class UserListActivity : BaseActivity(), UserListActionListener, UserListContract.View, Injectable {

    companion object {
        const val EXTRA_USERS = "users"
        const val EXTRA_TITLE = "title"
    }

    @Inject
    lateinit var presenter: UserListContract.Presenter

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var adapter: UserListAdapter
    private lateinit var users: List<User>
    private lateinit var pictures: MutableList<Bitmap?>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attach(this)
        rootLayoutID = R.id.user_list_content
        setContentView(R.layout.activity_user_list)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title =
            if (intent.hasExtra(EXTRA_TITLE)) intent.getStringExtra(EXTRA_TITLE)
            else ""

        users =
            if (intent.hasExtra(EXTRA_USERS)) intent.getSerializableExtra(EXTRA_USERS) as ArrayList<User>
            else emptyList()
        pictures = MutableList(users.size) { null }

        layoutManager = LinearLayoutManager(this)
        recycler_view.layoutManager = layoutManager
        adapter = UserListAdapter(users, pictures, this, this)
        recycler_view.adapter = adapter

        users.forEach { presenter.loadUserPicture(it) }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun showErrorMessage(error: String) {
        showMessage(error, duration = Snackbar.LENGTH_LONG)
    }

    override fun displayUserPicture(user: User, picture: Bitmap) {
        val idx = users.indexOf(user)
        if (idx in 0..users.size) {
            pictures[idx] = picture
            adapter.notifyDataSetChanged()
        }
    }

    override fun onViewUser(user: User) {
        startActivity(Intent(this, ProfileActivity::class.java).apply {
            putExtra(ProfileActivity.EXTRA_USER, user)
        })
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }
}
