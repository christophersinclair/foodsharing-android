package de.foodsharing.ui.baskets

import de.foodsharing.model.Basket

interface BasketActionListener {
    fun onViewBasket(basket: Basket)
}