package de.foodsharing.ui.baskets

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import de.foodsharing.R
import de.foodsharing.model.Basket
import de.foodsharing.utils.getDisplayName
import de.foodsharing.utils.inflate
import kotlinx.android.synthetic.main.item_basket.view.item_creation_date
import kotlinx.android.synthetic.main.item_basket.view.item_description
import kotlinx.android.synthetic.main.item_basket.view.item_distance
import kotlinx.android.synthetic.main.item_basket.view.item_title_text

class BasketListAdapter(
    private val listener: BasketActionListener,
    private val formatter: BasketDateFormatter,
    val context: Context
) : RecyclerView.Adapter<BasketListAdapter.BasketHolder>() {
    private var myBaskets: List<BasketItemModel> = emptyList()
    private var nearbyBaskets: List<BasketItemModel> = emptyList()

    private fun <T> getItem(position: Int, list1: List<T>, list2: List<T>): T {
        if (position < list1.size) {
            return list1[position]
        } else {
            return list2[position - list1.size]
        }
    }

    fun setBaskets(newMyBaskets: List<BasketItemModel>, newNearbyBaskets: List<BasketItemModel>) {
        val diffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldBasket = getItem(oldItemPosition, myBaskets, nearbyBaskets)
                val newBasket = getItem(newItemPosition, newMyBaskets, newNearbyBaskets)
                return oldBasket.basket.id == newBasket.basket.id
            }

            override fun getOldListSize(): Int {
                return myBaskets.size + nearbyBaskets.size
            }

            override fun getNewListSize(): Int {
                return newMyBaskets.size + newNearbyBaskets.size
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldBasket = getItem(oldItemPosition, myBaskets, nearbyBaskets)
                val newBasket = getItem(newItemPosition, newMyBaskets, newNearbyBaskets)
                return oldBasket.basket == newBasket.basket
            }
        })

        myBaskets = newMyBaskets
        nearbyBaskets = newNearbyBaskets
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int = myBaskets.size + nearbyBaskets.size

    override fun onBindViewHolder(holder: BasketHolder, position: Int) {
        if (position in 0 until itemCount) {
            val item = getItem(position, myBaskets, nearbyBaskets)
            holder.bind(item, position < myBaskets.size)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): BasketHolder {
        return BasketHolder(
                parent.inflate(
                        R.layout.item_basket,
                        false
                ), listener, formatter, context
        )
    }

    class BasketHolder(
        val view: View,
        private val listener: BasketActionListener,
        private val formatter: BasketDateFormatter,
        val context: Context
    ) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private var basket: Basket? = null

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            basket?.let {
                listener.onViewBasket(it)
            }
        }

        fun bind(item: BasketItemModel, isMyBasket: Boolean) {
            val basket = item.basket
            this.basket = basket

            view.item_creation_date.text = formatter.format(basket.createdAt)
            view.item_description.text = basket.description

            if (isMyBasket) {
                view.setBackgroundColor(
                    ContextCompat.getColor(context, R.color.mybasketBackgroundColor)
                )
                view.item_title_text.text = context.getString(R.string.basket_title_mine)
            } else {
                view.background = null
                view.item_title_text.text = context.getString(R.string.basket_title, basket.creator.getDisplayName(context))
            }

            if (item.distance != null) {
                view.item_distance.visibility = View.VISIBLE
                // TODO properly format this
                view.item_distance.text = "%.1f km".format(item.distance / 1000)
            } else {
                view.item_distance.visibility = View.GONE
            }
        }
    }
}