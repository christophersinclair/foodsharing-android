package de.foodsharing.ui.base

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import de.foodsharing.ui.login.LoginActivity
import de.foodsharing.utils.INTENT_EXTRA_GO_BACK
import de.foodsharing.utils.INTENT_EXTRA_REQUEST_CODE
import de.foodsharing.utils.SHARED_PREFERENCE_LOGGED_IN
import javax.inject.Inject

@SuppressLint("Registered")
open class AuthRequiredBaseActivity : BaseActivity() {

    companion object {
        const val REQUEST_LOGIN = 0
    }

    @Inject
    lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // starts login activity if not logged in
        if (!preferences.getBoolean(SHARED_PREFERENCE_LOGGED_IN, false)) {
            startActivityForResult(Intent(this, LoginActivity::class.java).apply {
                putExtra(INTENT_EXTRA_GO_BACK, true)
                putExtra(INTENT_EXTRA_REQUEST_CODE, REQUEST_LOGIN)
            }, REQUEST_LOGIN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_LOGIN -> if (resultCode == RESULT_CANCELED) {
                finish()
            }
        }
    }
}