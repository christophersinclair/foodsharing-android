package de.foodsharing.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import de.foodsharing.ui.basket.BasketViewModel
import de.foodsharing.ui.baskets.BasketsViewModel
import de.foodsharing.ui.conversation.ConversationViewModel
import de.foodsharing.ui.conversations.ConversationsViewModel
import de.foodsharing.ui.fsp.FairSharePointViewModel
import de.foodsharing.ui.login.LoginViewModel
import de.foodsharing.ui.main.MainViewModel
import de.foodsharing.ui.map.MapViewModel
import de.foodsharing.ui.profile.ProfileViewModel

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: DaggerViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BasketsViewModel::class)
    abstract fun bindBasketsViewModel(viewModel: BasketsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FairSharePointViewModel::class)
    abstract fun bindFSPViewModel(fspViewModel: FairSharePointViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    abstract fun bindProfileViewModel(profileViewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    abstract fun bindMapViewModel(mapViewModel: MapViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ConversationViewModel::class)
    abstract fun bindConversationViewModel(viewModel: ConversationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ConversationsViewModel::class)
    abstract fun bindConversationsViewModel(viewModel: ConversationsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BasketViewModel::class)
    abstract fun bindBasketViewModel(viewModel: BasketViewModel): ViewModel
}
