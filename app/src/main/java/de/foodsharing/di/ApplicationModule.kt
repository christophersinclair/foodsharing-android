package de.foodsharing.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.franmontiel.persistentcookiejar.ClearableCookieJar
import com.franmontiel.persistentcookiejar.PersistentCookieJar
import com.franmontiel.persistentcookiejar.cache.SetCookieCache
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import de.foodsharing.BuildConfig
import de.foodsharing.FoodsharingApplication
import de.foodsharing.api.AuthAPI
import de.foodsharing.api.BasketAPI
import de.foodsharing.api.ConversationsAPI
import de.foodsharing.api.DateDeserializer
import de.foodsharing.api.DefaultWebsocketAPI
import de.foodsharing.api.FairSharePointAPI
import de.foodsharing.api.MapAPI
import de.foodsharing.api.PopupAPI
import de.foodsharing.api.ProfileAPI
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.CACHE_SIZE
import de.foodsharing.utils.CSRFInterceptor
import de.foodsharing.utils.UnauthorizedRedirectInterceptor
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.Date
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun provideApplication(app: FoodsharingApplication): Application = app

    @Provides
    @Singleton
    fun provideContext(app: Application): Context = app

    @Provides
    @Singleton
    fun provideSharedPreferences(app: Application): SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(app)

    @Provides
    @Singleton
    fun provideCookieJar(context: Context): ClearableCookieJar {
        return PersistentCookieJar(SetCookieCache(), SharedPrefsCookiePersistor(context))
    }

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder()
            .registerTypeAdapter(Date::class.java, DateDeserializer())
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()

    @Provides
    @Singleton
    fun provideHttpClient(
        context: Context,
        cookieJar: ClearableCookieJar,
        csrfInterceptor: CSRFInterceptor,
        unauthorizedRedirectInterceptor: UnauthorizedRedirectInterceptor
    ): OkHttpClient = OkHttpClient.Builder()
            .cookieJar(cookieJar)
            .apply {
                if (BuildConfig.ENABLE_HTTP_LOGGING) {
                    addInterceptor(HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    })
                }
            }
            .cache(Cache(context.cacheDir, CACHE_SIZE))
            .addInterceptor(csrfInterceptor)
            .addInterceptor(unauthorizedRedirectInterceptor)
            .build()

    @Provides
    @Singleton
    fun provideRetrofit(
        gson: Gson,
        httpClient: OkHttpClient
    ): Retrofit = retrofit2.Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BASE_URL)
            .client(httpClient)
            .build()

    @Provides
    @Singleton
    fun provideWebsocketAPI(ws: DefaultWebsocketAPI): WebsocketAPI = ws

    @Provides
    @Singleton
    fun provideAuthAPI(retrofit: Retrofit): AuthAPI = retrofit.create(AuthAPI::class.java)

    @Provides
    @Singleton
    fun provideConversationsAPI(retrofit: Retrofit): ConversationsAPI =
            retrofit.create(ConversationsAPI::class.java)

    @Provides
    @Singleton
    fun provideMapAPI(retrofit: Retrofit): MapAPI = retrofit.create(MapAPI::class.java)

    @Provides
    @Singleton
    fun provideBasketAPI(retrofit: Retrofit): BasketAPI = retrofit.create(BasketAPI::class.java)

    @Provides
    @Singleton
    fun provideProfileAPI(retrofit: Retrofit): ProfileAPI = retrofit.create(ProfileAPI::class.java)

    @Provides
    @Singleton
    fun provideFairSharePointAPI(retrofit: Retrofit): FairSharePointAPI = retrofit.create(FairSharePointAPI::class.java)

    @Provides
    @Singleton
    fun providePopupAPI(retrofit: Retrofit): PopupAPI = retrofit.create(PopupAPI::class.java)
}