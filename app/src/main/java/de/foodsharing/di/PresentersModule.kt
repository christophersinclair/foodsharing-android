package de.foodsharing.di

import dagger.Module
import dagger.Provides
import de.foodsharing.ui.editbasket.EditBasketContract
import de.foodsharing.ui.editbasket.EditBasketPresenter
import de.foodsharing.ui.newbasket.NewBasketContract
import de.foodsharing.ui.newbasket.NewBasketPresenter
import de.foodsharing.ui.users.UserListContract
import de.foodsharing.ui.users.UserListPresenter

@Module
class PresentersModule {

    @Provides
    fun provideNewBasketPresenter(presenter: NewBasketPresenter): NewBasketContract.Presenter = presenter

    @Provides
    fun provideUserListPresenter(presenter: UserListPresenter): UserListContract.Presenter = presenter

    @Provides
    fun provideEditBasketPresenter(presenter: EditBasketPresenter): EditBasketContract.Presenter = presenter
}