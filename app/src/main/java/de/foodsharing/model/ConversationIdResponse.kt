package de.foodsharing.model

data class ConversationIdResponse(
    val data: ConversationIdData
)

data class ConversationIdData(
    val cid: Int
)