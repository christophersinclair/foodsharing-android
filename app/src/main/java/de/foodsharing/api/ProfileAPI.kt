package de.foodsharing.api

import de.foodsharing.model.Coordinate
import de.foodsharing.model.User
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

data class Profile(
    val id: Int,
    val name: String,
    val lastname: String,
    val address: String?,
    val city: String?,
    val postcode: String?,
    val lat: String?,
    val lon: String?,
    val email: String,
    val landline: String?,
    val mobile: String?
) {
    fun getCoordinates(): Coordinate? {
        return if (lat != null && lon != null) {
            Coordinate(lat.toDouble(), lon.toDouble())
        } else {
            null
        }
    }
}

/**
 * Retrofit API interface for user profiles
 */
interface ProfileAPI {
    /**
     * Fetches the profile of the currently logged in user
     */
    @GET("/api/profile/current")
    fun current(@Header("Cache-Control") cacheControl: String): Observable<Profile>

    /**
     * Fetches the profile of the user with the given id.
     */
    @GET("/api/user/{id}")
    fun getUser(@Path("id") id: Int): Observable<User>
}
