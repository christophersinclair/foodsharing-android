package de.foodsharing.utils

import android.content.Context
import de.foodsharing.R
import de.foodsharing.model.User

fun User.getDisplayName(context: Context): String {
    return this.name ?: context.getString(R.string.deleted_user)
}